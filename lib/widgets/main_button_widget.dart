import 'package:flutter/material.dart';

class MainButtonWidget extends StatelessWidget {
  String label;
  Color backgroundColor;
  Color foregroundColor;
  final executeAction;
  bool isEnable;
  bool isLoading;

  MainButtonWidget({
    Key? key,
    required this.label,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.executeAction,
    this.isEnable = true,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            backgroundColor: isEnable ? backgroundColor : Colors.grey),
        child: Container(
          width: double.infinity,
          alignment: Alignment.center,
          height: 50,
          child: isLoading
              ? SizedBox(
                  width: 30,
                  height: 30,
                  child: CircularProgressIndicator(backgroundColor: Colors.white),
                )
              : Text(
                  label,
                  style: TextStyle(color: foregroundColor, fontSize: 16, fontWeight: FontWeight.w400),
                ),
        ),
        onPressed: () {
          if (isLoading == false && isEnable == true) {
            executeAction();
          }
        },
      ),
    );
  }
}
