import 'package:connectivity_checker/connectivity_checker.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';


class ConnectionChecker {

  Future<bool> isOnline() async{
 
    if(await ConnectivityWrapper.instance.isConnected == false){
      Get.snackbar('You_do_not_have_an_internet_connection'.tr,'You_do_not_have_an_internet_connection'.tr,snackPosition: SnackPosition.BOTTOM);
      return false;
    }

    return true;

  }

}