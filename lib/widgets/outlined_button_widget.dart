import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class OutlinedButtonWidget extends StatelessWidget{
  String label;
  Color backgroundColor;
  Color foregroundColor;
  Color strokerColor;
  final executeAction;
  OutlinedButtonWidget({Key? key, required this.label,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.strokerColor,
    required this.executeAction
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
          return OutlinedButton(
            style:
            OutlinedButton.styleFrom(
                side: BorderSide(width: 1, color: strokerColor),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)
                ),
                backgroundColor: backgroundColor,

            ),
            child: Container(
              height: 50,
              alignment: Alignment.center,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset(
                  'images/google_svg.svg',
                  width: 24,
                  height: 24,
                  semanticsLabel: 'Una imagen SVG'),
                  Text(label,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: foregroundColor
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
            onPressed: () {
              executeAction();
            },
          );
  }
}