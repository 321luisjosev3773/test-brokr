// To parse this JSON data, do
//
//     final success = successFromJson(jsonString);

import 'dart:convert';

Success successFromJson(String str) => Success.fromJson(json.decode(str));

String successToJson(Success data) => json.encode(data.toJson());

class Success {
  String status;
  String message;
  List<dynamic> data;

  Success({
    required this.status,
    required this.message,
    required this.data,
  });

  factory Success.fromJson(Map<String, dynamic> json) => Success(
    status: json["status"],
    message: json["message"],
    data: List<dynamic>.from(json["data"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x)),
  };
}
