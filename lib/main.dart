import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:test_brokr/pages/dashboard/screen/dashboard_screen.dart';
import 'package:test_brokr/pages/login/screen/login_screen.dart';
import 'package:test_brokr/pages/sign/screen/complete_sign_screen.dart';
import 'package:test_brokr/pages/sign/screen/confirmed_screen.dart';
import 'package:test_brokr/pages/sign/screen/uploda_photo_screen.dart';
import 'package:test_brokr/translations/Messages.dart';
import 'firebase/firebase_options.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );


  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      translations: Messages(),
      locale: Locale('es','ES'),
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      initialRoute: '/',
      getPages: [
        GetPage(name: '/', page: () => LoginScreen()),
        GetPage(name: '/dashboard', page: () => Dashboard()),
        GetPage(name: '/register', page: () => CompleteSignScreen()),
        GetPage(name: '/uploadPhoto', page: () => UploadPhotoScreen()),
        GetPage(name: '/comfirmed', page: () => ConfirmedScreen()),
      ],
    );
  }
}
