




import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ImageHelper {

  ImageHelper({
    ImagePicker? imagePicker,
    ImageCropper? imageCropper,
  }) : _imagePicker = imagePicker ?? ImagePicker(),
      _imageCropper = imageCropper ?? ImageCropper();

  final ImagePicker _imagePicker;
  final ImageCropper _imageCropper;

  Future<List<XFile>> pickImage({
    ImageSource source = ImageSource.gallery,
    int imageQuality = 100,
    bool multiple = false,
  }) async {
    if(multiple) {
      return await _imagePicker.pickMultiImage(imageQuality: imageQuality);  
    }
    final file = await _imagePicker.pickImage(
      source: source,
      imageQuality: imageQuality,
      );

    if(file != null) return [file];
    return[];

  }

  Future<CroppedFile?> crop({
    required XFile file,
    CropStyle cropStyle = CropStyle.circle,
  }) async => 
      await _imageCropper.cropImage(
        cropStyle: cropStyle,
        sourcePath: file.path,
        
      );


  Future<String> base64string({
    required File imagefile,
  }) async {
    String imagepath = " /data/user/img.jpg"; 
    //image path, you can get it with image_picker package

    //File imagefile = file; //convert Path to File
    Uint8List imagebytes = await imagefile.readAsBytes(); //convert to bytes
    String base64string = base64.encode(imagebytes); //convert bytes to base64 string

    print(base64string); 

    return base64string;
  }


}

