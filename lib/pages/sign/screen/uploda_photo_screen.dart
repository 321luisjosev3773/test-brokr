import 'dart:io';


import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:test_brokr/pages/sign/controller/complete_sign_controller.dart';
import 'package:test_brokr/pages/sign/screen/confirmed_screen.dart';
import 'package:test_brokr/widgets/header_tittle_widget.dart';
import 'package:test_brokr/widgets/main_button_widget.dart';
import '../../../utils/imageHelper.dart';
import '../../../widgets/header_tittle_widget.dart';
import '../../../widgets/main_button_widget.dart';

enum AppState {
  galery,
  photo,
  picker,
  cropper,
}

class UploadPhotoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UploadPhotoScreen();
  final imageHelper = ImageHelper();
}

class _UploadPhotoScreen extends State<UploadPhotoScreen> {
  late AppState state;
  File? _selectImage;
  final controllerSign = Get.put(CompleteSignController());

  @override
  void initState() {
    super.initState();
    state = AppState.galery;
  }

  Future _pickImageFromGallery() async {
    final response = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (response == null) return;
    state = AppState.picker;
    _selectImage = File(response.path);
    setState(() {
      stateImage();
    });
  }

  Future _pickImageFromCamera() async {
    final response = await ImagePicker().pickImage(source: ImageSource.camera);
    if (response == null) return;
    state = AppState.picker;
    _selectImage = File(response.path);
    setState(() {
      stateImage();
    });
  }

  savePhoto()async{
    Locale locale = Localizations.localeOf(context);
    final result = await controllerSign.savePhoto(_selectImage,locale.languageCode);
    if(result != null){
      if(result){
        Get.to(ConfirmedScreen());
      }
    }
  }

  Future _cropImage() async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: _selectImage!.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Edit_photo'.tr,
            toolbarColor: Color(0xff675DFE),
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        IOSUiSettings(
          title: 'Cropper'.tr,
        ),
        WebUiSettings(
          context: context,
        ),
      ],
    );
    if (croppedFile != null) {
      _selectImage = File(croppedFile.path);
      setState(() {});
    }
  }

  _clearImage() {
    _selectImage = null;
  }

  stateImage() {
    if (state == AppState.galery) {
      _pickImageFromGallery();
    } else if (state == AppState.picker) {
      _cropImage();
    } else if (state == AppState.photo) {
      _pickImageFromCamera();
    } else if (state == AppState.cropper) {
      _clearImage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            HeaderTittleWidget(tittle: 'Profile_photo'.tr),
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                _selectImage == null
                    ? Container(
                    height: 250,
                    width: 250,
                    decoration: BoxDecoration(
                        color: Colors.white, shape: BoxShape.circle),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: ClipOval(
                          child:
                          SvgPicture.asset("images/default_photo.svg")),
                    ))
                    : Container(
                  height: 250,
                  width: 250,
                  decoration: BoxDecoration(
                      color: Colors.grey, shape: BoxShape.circle),
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    backgroundImage: FileImage(_selectImage!),
                  ),
                ),
                InkWell(
                  onTap: () {
                    showModalPopup();
                  },
                  child: Container(
                      width: 120,
                      height: 45,
                      decoration: BoxDecoration(
                          color: Color(0xffEFEFEF),
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      transform: Matrix4.translationValues(0, 20, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset("images/camara_flat.svg"),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Upload'.tr,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                                color: Color(0xff505050)),
                          )
                        ],
                      )),
                )
              ],
            ),
            SizedBox(),
            BottomPhotoAction()
          ],
        ),
      ),
    );
  }

  showModalPopup() {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) =>
            Container(
              height: 200,
              padding: const EdgeInsets.only(top: 6.0),
              margin: EdgeInsets.only(
                bottom: MediaQuery
                    .of(context)
                    .viewInsets
                    .bottom,
              ),
              color: Colors.transparent,
              child: SafeArea(
                top: false,
                child: Container(
                  padding: EdgeInsets.all(10),
                  color: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      actionPhoto(),
                      MainButtonWidget(
                          label: 'Cancel'.tr,
                          backgroundColor: Colors.white,
                          foregroundColor: Color(0xffFF3B30),
                          executeAction: () => Get.back())
                    ],
                  ),
                ),
              ),
            ));
  }

  Widget actionPhoto() {
    return Column(
      children: [
        MainButtonWidget(
            label: 'Choose_from_photo_library'.tr,
            backgroundColor: Colors.white,
            foregroundColor: Color(0xff007AFF),
            executeAction: () =>
            {
              setState(() { state = AppState.galery; }),
              stateImage(),
              Get.back()
            }),
        SizedBox(height: 1,),
        MainButtonWidget(
            label: 'Take_a_photo'.tr,
            backgroundColor: Colors.white,
            foregroundColor: Color(0xffF007AFF),
            executeAction: () =>  {
              setState(() { state = AppState.photo; }),
              stateImage(),
              Get.back()
            }),
      ],
    );
  }
  Widget BottomPhotoAction() {
    return Container(
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          Divider(),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: TextButton(
                  onPressed: () => Get.back(),
                  child: Text(
                    'Skip'.tr,
                    style: TextStyle(
                        color: Color(0xff675DFE),
                        fontSize: 15,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
              Flexible(
                child: MainButtonWidget(
                    label: 'save'.tr,
                    backgroundColor: Color(0xff675DFE),
                    foregroundColor: Colors.white,
                    executeAction: savePhoto),
              ),
            ],
          )
        ],
      ),
    );
  }
}


