import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../../../utils/ConnectionCheckerService.dart';
import '../../../utils/status_service.dart';
import '../models/DataLogin.dart';
import '../services/login_services.dart';

class LoginGoogleController extends GetxController {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final _loginServices = LoginServices();
  final statusService = StatusService();
  final connectionChecker = ConnectionChecker();

  String? _emailGoogle;
  String? get emailGoogle => _emailGoogle;

  String? _nameGoogle;
  String? get nameGoogle => _nameGoogle;

  String? _phoneGoogle;
  String? get phoneGoogle => _phoneGoogle;

  String? _tokenGoogle;
  String? get tokenGoogle => _tokenGoogle;

  DataLogin? dataLogin;

  updateSocial(){
    _emailGoogle = null;
    _nameGoogle = null;
    _phoneGoogle = null;
    _tokenGoogle = null;
    update();
  }

  signInWithGoogle() async {
    print("signInWithGoogle");
    updateSocial();
    try {
          await googleSignIn.signOut();
      final GoogleSignInAccount? googleSignInAccount =
          await googleSignIn.signIn();
          print("googleSignInAccount" +googleSignInAccount.toString());
      if (googleSignInAccount != null) {
        googleSignInAccount.email;
        googleSignInAccount.displayName;
        googleSignInAccount.photoUrl;

        print("paso") ;
        final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

         _emailGoogle = googleSignInAccount.email;
         _nameGoogle = googleSignInAccount.displayName;
         _phoneGoogle = googleSignInAccount.photoUrl;
         _tokenGoogle = googleSignInAuthentication.accessToken;
         print("accessToken" + _tokenGoogle.toString());
         update();
      }
    } catch (e) {

    }
  }


  loginSocial(local,)async{
    if (await connectionChecker.isOnline() == false) {
    }else{
      var response =  await _loginServices.loginWithSocial(_tokenGoogle, local);
      if(response["isSuccess"]){
        if(response["data"]["data"]["status"] == "failure"){
          Get.snackbar(response["data"]["message"],
              response["data"]["data"]["data"]["errors"]["error"][0],
              snackPosition: SnackPosition.BOTTOM);
          return null;
        }else{
          dataLogin = dataLoginFromJson(response["data"]["data"]["data"]);
          if(dataLogin?.customer.completedProfile != "Y"){
            return false;
          }else{
            return true;
          }
        }
      }else{
        Get.snackbar("Error", response["data"]["message"],snackPosition: SnackPosition.BOTTOM);
        return null;
      }
    }
  }
}
