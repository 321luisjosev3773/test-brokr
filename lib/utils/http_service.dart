import 'dart:convert';
import 'dart:io';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:test_brokr/utils/status_service.dart';
import 'package:test_brokr/utils/userService.dart';
import '../pages/login/models/Failure.dart';
import '../pages/login/models/Success.dart';
import '../utils/oauth_token_service.dart';

class HttpService {
  final client = http.Client();
  final oauthTokenService = OauthTokenService();
  final userService = FirebaseShared();
  final statusService = StatusService();


  httpPost(String url, data, {bool isClientAuth = false}) async {
    final headers = await setHeaderRequest(isClientAuth);
    await dotenv.load(
      fileName: ".env",
    );
    final response = await client.post(Uri.parse("${dotenv.env['SERVER_URL']}$url"), headers: headers, body: json.encode(data));
    print(response.body);
    var hasError = await responseHasError(response);
    if(hasError != false){
      if(hasError["status"] == statusService.NOT_FOUND ||
          hasError["status"] == statusService.ERROR){
        return {"status": hasError["status"], "message": hasError["message"], "data": {}};
      }
    }
    return {"status": response.statusCode, "message": json.decode(response.body)["message"], "data": json.decode(response.body)};

  }

  httpPostMultipart(String ruta,Map<String, String> data,File? photo) async {
    await dotenv.load(
      fileName: ".env",
    );
    var request = http.MultipartRequest('POST', Uri.parse("${dotenv.env['SERVER_URL']}$ruta"));

    request.files.add(await http.MultipartFile.fromPath(
      'avatar',
      photo?.path ?? "",
      filename: photo?.path ?? "Screenshot.jpg",
    ));
    request.fields.addAll(data);
    var response = await request.send();
    final respStr = await response.stream.bytesToString();
    print(
      jsonDecode(respStr),
    );
    return jsonDecode(respStr);
  }


  responseHasError(response) {
    if (response.statusCode == statusService.UNAUTHORIZED) {
      return {"status": statusService.UNAUTHORIZED, "message": 'You_are_not_authenticated'.tr};
    } else if (response.statusCode == statusService.NOT_FOUND) {
      return {"status": statusService.NOT_FOUND, "message": 'resource_not_found'.tr};
    } else if (response.statusCode == statusService.ERROR) {
      return {"status": statusService.ERROR, "message": 'something_has_gone_wrong'.tr};
    } else if (response.statusCode == statusService.FORBIDDEN) {
      return {"status": statusService.FORBIDDEN, "message": 'forbidden'.tr};
    } else if (response.statusCode == statusService.VALIDATION_FAIL) {
      return {"status": statusService.VALIDATION_FAIL, "message": 'Incorrect_data'.tr};
    } else if (response.statusCode == statusService.BADREQUEST) {
    return {"status": statusService.BADREQUEST, "message": response};
    }
    return false;
  }

  setHeaderRequest(bool isClientAuth) async {
    if (isClientAuth) {
      return {"Accept": "application/json", "Content-type": "application/json"};
    } else {
      final accessToken = await oauthTokenService.getOauthToken();
      return {"Accept": "application/json", "Content-type": "application/json", "Authorization": "Bearer ${accessToken}"};
    }
  }

}
