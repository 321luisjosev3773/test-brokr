import 'package:flutter/material.dart';

class TextFieldPasswordWidget extends StatefulWidget{
  String label;
  TextInputType type;
  TextEditingController controller ;
  bool hasError ;
  String? errorMessage;
  TextFieldPasswordWidget({
    Key? key,
    required this.label,
    required this.type,
    required this.controller,
    required this.hasError,
    required this.errorMessage,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TextFieldPasswordWidget();

}
class _TextFieldPasswordWidget extends State<TextFieldPasswordWidget>{
  bool click = true;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      keyboardType: widget.type,
      obscureText: click,
      decoration: InputDecoration(
          errorText: widget.errorMessage,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8)
          ),
          label: Text(widget.label, style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: Color(0xff75788D)
          ),),
          suffixIcon:IconButton(
            icon: click == true ? Icon(color: Color(0xff5A6684),  Icons.visibility_off) : Icon(color: Color(0xff5A6684), Icons.visibility),
            onPressed: () {
              setState(() {
                click = !click;
              });
            },
          )),
      style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          color: Color(0xff75788D)
      ),
      onChanged: (value) {},
    );
  }

}