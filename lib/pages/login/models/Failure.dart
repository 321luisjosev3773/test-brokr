// To parse this JSON data, do
//
//     final failure = failureFromJson(jsonString);

import 'dart:convert';

Failure failureFromJson(Map<String, dynamic> str) => Failure.fromJson(str);

String failureToJson(Failure data) => json.encode(data.toJson());

class Failure {
  String status;
  String message;
  Data data;

  Failure({
    required this.status,
    required this.message,
    required this.data,
  });

  factory Failure.fromJson(Map<String, dynamic> json) => Failure(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Errors errors;

  Data({
    required this.errors,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    errors: Errors.fromJson(json["errors"]),
  );

  Map<String, dynamic> toJson() => {
    "errors": errors.toJson(),
  };
}

class Errors {
  List<String> error;

  Errors({
    required this.error,
  });

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
    error: List<String>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}