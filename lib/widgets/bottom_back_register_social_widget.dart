import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class BottomBackRegisterSocialWidget extends StatelessWidget {
  String label;
  Color backgroundColor;
  Color foregroundColor;
  Color strokerColor;
  final executeAction;
  BottomBackRegisterSocialWidget({Key? key, required this.label,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.strokerColor,
    required this.executeAction
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Flexible(child: Divider()),
            SizedBox(
              width: 5,
            ),
            Text('Start_with_a_different_account'.tr, style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 14
            ),),
            SizedBox(
              width: 5,
            ),
            Flexible(child: Divider())
          ],
        ),
        SizedBox(
          height: 24,
        ),
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(width: 1, color: strokerColor),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            backgroundColor: backgroundColor,
          ),
          child: Container(
            height: 50,
            alignment: Alignment.center,
            width: double.infinity,
            child: Text(
              label,
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: foregroundColor),
            ),
          ),
          onPressed: () {
            executeAction();
          },
        )
      ],
    );
  }
}
