import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:test_brokr/pages/login/controllers_login/login_google_controller.dart';
import 'package:test_brokr/pages/login/controllers_login/login_with_password_controller.dart';
import 'package:test_brokr/pages/sign/screen/uploda_photo_screen.dart';
import 'package:test_brokr/widgets/header_tittle_widget.dart';
import 'package:test_brokr/widgets/text_field_password_widget.dart';
import 'package:test_brokr/widgets/text_field_widget.dart';

import '../../../widgets/main_button_widget.dart';
import '../controller/complete_sign_controller.dart';
import 'package:country_picker/country_picker.dart';

class CompleteSignScreen extends StatefulWidget {
  final Map<String, dynamic> args = Get.arguments;

  CompleteSignScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CompleteSignScreen();
}

class _CompleteSignScreen extends State<CompleteSignScreen> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();
  TextEditingController controllerCountry = TextEditingController();
  TextEditingController controllerBirthday = TextEditingController();
  TextEditingController controllerPhone = TextEditingController();

  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  TextEditingController controllerRepeatPassword = TextEditingController();

  DateTime valor = DateTime.now();
  final signController = Get.put(CompleteSignController());
  final _loginGoogleController = Get.put(LoginGoogleController());
  final _loginWithPasswordController = Get.put(LoginWithPasswordController());

  @override
  initState() {
    super.initState();
    if (widget.args["isSocial"]) {
      controllerEmail.text = _loginGoogleController.emailGoogle ?? "";
      controllerName.text = _loginGoogleController.nameGoogle ?? "";
      controllerPhone.text = _loginGoogleController.phoneGoogle ?? "";
      _loginGoogleController.tokenGoogle;
    } else {
      controllerEmail.text = _loginWithPasswordController.email ?? "";
      controllerPassword.text = _loginWithPasswordController.password ?? "";
    }
    controllerBirthday.text = "mm/dd/yyyy";
    signController.setIsSocial(widget.args["isSocial"]);
    check();
  }

  check() {
    verifyEmail();
    controllerPhone.addListener(() {
      verifyPhone();
    });
    controllerEmail.addListener(() {
      verifyEmail();
    });
  }

  verifyPhone() async {
    await signController.verifyPhone(controllerPhone.text);
  }

  verifyEmail() async {
    await signController.checkEmail(controllerEmail.text);
  }

  signUp() async {
    final response = await signController.signData(
      controllerName.text,
      controllerLastName.text,
      controllerBirthday.text,
      saveCodeCountry,
      controllerPhone.text,
      saveCodePhone,
      controllerEmail.text,
      controllerPassword.text,
      controllerRepeatPassword.text,
    );
    if (response) {
      Get.to(UploadPhotoScreen());
    }
  }

  _showDialog(Widget child) {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
                color: CupertinoColors.systemBackground.resolveFrom(context),
              ),
              height: 250,
              padding: const EdgeInsets.only(top: 6.0),
              margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: SafeArea(
                top: false,
                child: Column(
                  children: [
                    Flexible(child: headerDatePicker()),
                    Flexible(child: child)
                  ],
                ),
              ),
            ));
  }

  Widget headerDatePicker() {
    return Container(
      height: 80,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 10,
            width: 100,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Color(0xffd7d7d7)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: TextButton(
                    onPressed: () => Get.back(),
                    child: Text(
                      'Cancel'.tr,
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff007AFF)),
                    ),
                  ),
                ),
                Text('Select_your_birthday'.tr,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.black87)),
                Flexible(
                  child: TextButton(
                    onPressed: () => Get.back(),
                    child: Text(
                      'Done'.tr,
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff007AFF)),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider()
        ],
      ),
    );
  }

  calendar() async {
    _showDialog(
      CupertinoDatePicker(
        maximumDate: DateTime.now(),
        initialDateTime: valor,
        mode: CupertinoDatePickerMode.date,
        use24hFormat: true,
        onDateTimeChanged: (DateTime newDate) {
          setState(() {
            valor = newDate;
            controllerBirthday.text =
                "${valor.month}/${valor.day}/${valor.year}";
          });
        },
      ),
    );
  }

  String codePhone = "";
  String saveCodePhone = "";
  String saveCodeCountry = "";

  countriesShow() {
    showCountryPicker(
      context: context,
      showPhoneCode: true,
      // optional. Shows phone code before the country name.
      onSelect: (Country country) {
        setState(() {
          controllerCountry.text = country.displayName.toString();
          saveCodePhone = country.phoneCode.toString();
          saveCodeCountry = country.countryCode.toString();
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.only(top: 30),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  HeaderTittleWidget(tittle: 'Complete_sign_up'.tr),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        GetBuilder<CompleteSignController>(
                          init: CompleteSignController(),
                          builder: (_) => TextFieldWidget(
                              label: 'Name'.tr,
                              type: TextInputType.name,
                              controller: controllerName,
                              hasError: _.nameHasError,
                              errorMessage: _.nameMessageError),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GetBuilder<CompleteSignController>(
                          init: CompleteSignController(),
                          builder: (_) => TextFieldWidget(
                              label: 'Last_Name'.tr,
                              type: TextInputType.name,
                              controller: controllerLastName,
                              hasError: _.lastNameHasError,
                              errorMessage: _.lastNameMessageError),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFieldBirthdayWidget(controllerBirthday),
                        SizedBox(
                          height: 20,
                        ),
                        GetBuilder<CompleteSignController>(
                            init: CompleteSignController(),
                            builder: (_) => dropdownCountry(
                                controllerCountry,
                                TextInputType.name,
                                _.countryMessageError,
                                'Country_Region'.tr)),
                        SizedBox(
                          height: 20,
                        ),
                        GetBuilder<CompleteSignController>(
                            init: CompleteSignController(),
                            builder: (_) => TextFormField(
                                  controller: controllerPhone,
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    prefixIcon: saveCodePhone != ""
                                        ? Container(
                                            width: 40,
                                            alignment: Alignment.center,
                                            child: Text("+" + saveCodePhone,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14,
                                                  color: Color(0xff75788D),
                                                )),
                                          )
                                        : null,
                                    suffixIcon: _.phoneMessageError != null
                                        ? Icon(
                                            Icons.close,
                                            color: Colors.red,
                                          )
                                        : _.phoneHasError == true
                                            ? Icon(
                                                Icons.check,
                                                color: Colors.green,
                                              )
                                            : null,
                                    errorText: _.phoneMessageError,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8))),
                                    label: Text(
                                      'Number_Phone'.tr,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14,
                                          color: Color(0xff75788D)),
                                    ),
                                  ),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: Color(0xff75788D)),
                                  onChanged: (value) {},
                                )),
                        SizedBox(
                          height: 10,
                        ),
                        CompleteSignEmailAndPassword(),
                        SizedBox(
                          height: 20,
                        ),
                        bottomApp()
                      ],
                    ),
                  )
                ],
              ),
            )));
  }

  Widget dropdownCountry(controller, type, errorMessage, label) {
    return TextFormField(
      onTap: () => countriesShow(),
      readOnly: true,
      controller: controller,
      keyboardType: type,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        suffixIcon: Icon(Icons.arrow_drop_down),
        errorText: errorMessage,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8))),
        label: Text(
          label,
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: Color(0xff75788D)),
        ),
      ),
      style: TextStyle(
          fontWeight: FontWeight.w400, fontSize: 14, color: Color(0xff75788D)),
      onChanged: (value) {},
    );
  }

  Widget TextFieldBirthdayWidget(TextEditingController controller) {
    return GetBuilder<CompleteSignController>(
        init: CompleteSignController(),
        builder: (_) => TextFormField(
              readOnly: true,
              onTap: () => calendar(),
              controller: controller,
              keyboardType: TextInputType.datetime,
              decoration: InputDecoration(
                suffixIcon: Container(
                    padding: EdgeInsets.all(10),
                    child: SvgPicture.asset(
                      "images/calendar_regular.svg",
                    )),
                errorText: _.birthdayMessageError,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                label: Text(
                  'Birthday'.tr,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: Color(0xff75788D)),
                ),
              ),
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: Color(0xff75788D)),
              onChanged: (value) {},
            ));
  }

  Widget CompleteSignEmailAndPassword() {
    return Column(
      children: [
        Divider(),
        SizedBox(
          height: 10,
        ),
        GetBuilder<CompleteSignController>(
            init: CompleteSignController(),
            builder: (_) => TextFormField(
                  controller: controllerEmail,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    suffixIcon: _.emailMessageError != null
                        ? Icon(
                      Icons.close,
                      color: Colors.red,
                    )
                        : _.emailHasError == true
                        ? Icon(
                      Icons.check,
                      color: Colors.green,
                    )
                        : null,
                    errorText: _.emailMessageError,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    label: Text(
                      'email'.tr,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Color(0xff75788D)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: Color(0xff75788D)),
                  onChanged: (value) {},
                )),
        SizedBox(
          height: 20,
        ),
        widget.args["isSocial"] ?
        SizedBox()
            :
        containerPassword()
      ],
    );
  }
  Widget containerPassword(){
    return Column(
      children: [
        GetBuilder<CompleteSignController>(
          init: CompleteSignController(),
          builder: (_) => TextFieldPasswordWidget(
            hasError: _.passwordHasError,
            errorMessage: _.passwordMessageError,
            controller: controllerPassword,
            label: 'password'.tr,
            type: TextInputType.emailAddress,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        GetBuilder<CompleteSignController>(
          init: CompleteSignController(),
          builder: (_) => TextFieldPasswordWidget(
            hasError: _.repeatPasswordHasError,
            errorMessage: _.repeatPasswordMessageError,
            controller: controllerRepeatPassword,
            label: 'Repeat_Password'.tr,
            type: TextInputType.emailAddress,
          ),
        ),
      ],
    );
  }

  Widget bottomApp() {
    var text = RichText(
      text: TextSpan(
        style: const TextStyle(
          fontSize: 14.0,
          color: Color(0xff505050),
        ),
        children: <TextSpan>[
          TextSpan(
              text: 'By_clicking_continue_you_are_agreeing_to_our'.tr,
              style: TextStyle(
                fontWeight: FontWeight.w400,
              )),
          TextSpan(
              text: 'Privacy_Policy'.tr,
              style: TextStyle(
                fontWeight: FontWeight.w600,
              )),
          TextSpan(
            text: 'and'.tr,
            style: TextStyle(
              fontWeight: FontWeight.w400,
            ),
          ),
          TextSpan(
              text: 'Terms_&_Conditions'.tr,
              style: TextStyle(
                fontWeight: FontWeight.w600,
              )),
        ],
      ),
    );
    return Column(
      children: [
        text,
        SizedBox(
          height: 20,
        ),
        GetBuilder<CompleteSignController>(
          init: CompleteSignController(),
          builder: (_) => MainButtonWidget(
              isLoading: _.isLoading,
              label: 'Agree_and_continue'.tr,
              backgroundColor: Color(0xff675DFE),
              foregroundColor: Colors.white,
              executeAction: signUp),
        )
      ],
    );
  }
}
