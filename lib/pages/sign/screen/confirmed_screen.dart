import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:test_brokr/pages/login/screen/login_screen.dart';

import '../../../widgets/main_button_widget.dart';
import '../../dashboard/screen/dashboard_screen.dart';
import '../controller/complete_sign_controller.dart';

class ConfirmedScreen extends StatelessWidget {
    final controller = Get.put(CompleteSignController());
  onClick()async {
    Get.offAll(LoginScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(10.0),
        child: MainButtonWidget(
          label: 'continue'.tr,
          foregroundColor: Colors.white,
          backgroundColor: Color(0xff675DFE),
          executeAction: onClick,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset("images/congras_color.svg"),
          SizedBox(height: 10),
          Text(
            'Congratulations'.tr,
            style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: Colors.black87),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 50.0,right: 50),
            child: Text(
              'Thank_you_for_completing_your_details_now_you_can_access_the_platform'.tr,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                  color: Colors.black87),
            ),
          )
        ],
      ),
    );
  }
}
