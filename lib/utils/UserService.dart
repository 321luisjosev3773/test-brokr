import 'package:shared_preferences/shared_preferences.dart';

class UserServices {
  Future getUser() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString("user") ?? "";
  }
  void setUser(token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("user",token);
  }
}