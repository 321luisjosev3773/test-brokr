import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:test_brokr/pages/login/controllers_login/login_google_controller.dart';
import 'package:test_brokr/pages/login/controllers_login/login_with_password_controller.dart';
import 'package:test_brokr/utils/UserService.dart';

import '../../login/models/User.dart';

class Dashboard extends StatefulWidget{
  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final shared = UserServices();
  final controller = Get.put(LoginGoogleController());
  String? user;
  @override
  void initState() {
    getUser();
    super.initState();
  }
  getUser()async{
    final data = await shared.getUser();
    if(data == ""){
      setState(() {
        user =  controller.nameGoogle;
      });
    }else{
      setState(() {
        user = userFromJson(json.decode(data)).name;
      });
    }

    print("getUser" + data.toString());

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Image.asset("images/profile_image.png"),
                  SizedBox(width: 10,),
                  UserNameDashboard()
                ],
              ),
            ),
            SvgPicture.asset(
                'images/empty_verified.svg',
                width: 300,
                height: 300,
                semanticsLabel: 'Una imagen SVG'),
            SizedBox(height: 30,)
          ],
        ),
      ),
    );
  }
  Widget UserNameDashboard(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text('Hi'.tr,style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 20,
                color: Color(0xff505050)
            ),),
            SizedBox(width: 5,),
            Text(user ?? "", style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: Color(0xff505050)
            ),),

          ],
        ),
        Text('Good_Morning'.tr,
          style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Color(0xff505050)
          ),),
      ],
    );

  }
}
