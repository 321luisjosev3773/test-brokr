import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget{
  String label;
  TextInputType type;
  TextEditingController controller;
  bool hasError ;
  String? errorMessage;
  TextFieldWidget({
    Key? key,
    required this.label,
    required this.type,
    required this.controller,
    required this.hasError,
    required this.errorMessage,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
   return  TextFormField(
     controller: controller,
      keyboardType: type,
      decoration: InputDecoration(
        suffixIcon: errorMessage!=null ? Icon(Icons.close,color: Colors.red,) : null,
        errorText: errorMessage,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))
        ),
          label: Text(label, style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: Color(0xff75788D)
          ),),
      ),
      style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 14,
          color: Color(0xff75788D)
      ),
      onChanged: (value) {},
    );
  }

}