import 'dart:io';

import '../../../utils/http_service.dart';
import '../../../utils/status_service.dart';
import '../../login/models/DataLogin.dart';

class SignServices {
  final httpService = HttpService();
  final statusService = StatusService();


  register(data,File? photo,locale)async{
    final dataBody = {
      "name": data["name"].toString(),
      "last_name": data["lastName"].toString(),
      "birthdate": data["birthday"].toString(),
      "id_country": data["country"].toString(),
      "phone": data["phone"].toString(),
      "phone_code": data["phone_code"].toString(),
      "email": data["email"].toString(),
      "password": data["password"].toString(),
      "password_confirmation": data["password_confirmation"].toString(),
      "os": Platform.operatingSystem.toString(),
      "type": "guest",
      "language": locale.toString(),
    };
    print(dataBody);
    final response = await httpService.httpPostMultipart("api/auth/register", dataBody, photo);
    print(response);
    return response;
  }

  verifyPhone(phone)async{
    final dataBody = {
      "phone": phone,
    };
    print(dataBody);
    final response = await httpService.httpPost("api/check_phone", dataBody, isClientAuth: true);
    print("verifyPhone"+ response.toString());
    return {"isSuccess": response["status"] == statusService.SUCCESS ? true : false, "data": response,"status" : response["status"]};
  }

  checkEmail(data)async{
    data = {
      "email": data
    };
    final response = await httpService.httpPost("api/check_email", data, isClientAuth: true);
    if(response["status"] == 200){
      return {"isSuccess": true , "data": response["data"], "message": response["message"]};
    }else if(response["status"] == 400){
      return {"isSuccess": true , "data": response["data"], "message": response["message"]};
    }else{
      return {"isSuccess": false, "data": response["data"], "message": response["message"]};
    }
  }

  completeProfile(dataLogin)async{
    final response = await httpService.httpPost("api/profile", dataLogin);
    return {"isSuccess": response["status"] == statusService.SUCCESS ? true : false, "data": response,"status" : response["status"]};
  }
}