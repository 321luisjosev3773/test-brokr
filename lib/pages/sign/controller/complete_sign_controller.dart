import 'dart:io';

import 'package:get/get.dart';
import 'package:test_brokr/utils/UserService.dart';
import '../../../utils/ConnectionCheckerService.dart';
import '../../../utils/status_service.dart';
import '../../login/models/User.dart';
import '../services/sign_services.dart';
import 'package:intl/intl.dart';

class CompleteSignController extends GetxController {
  String? _emailMessageError;

  String? get emailMessageError => _emailMessageError;

  bool _emailHasError = false;

  bool get emailHasError => _emailHasError;

  String? _passwordMessageError;

  String? get passwordMessageError => _passwordMessageError;

  bool _passwordHasError = false;

  bool get passwordHasError => _passwordHasError;

  String? _repeatPasswordMessageError;

  String? get repeatPasswordMessageError => _repeatPasswordMessageError;

  bool _repeatPasswordHasError = false;

  bool get repeatPasswordHasError => _repeatPasswordHasError;

  String? _nameMessageError;

  String? get nameMessageError => _nameMessageError;

  bool _nameHasError = false;

  bool get nameHasError => _nameHasError;

  String? _lastNameMessageError;

  String? get lastNameMessageError => _lastNameMessageError;

  bool _lastNameHasError = false;

  bool get lastNameHasError => _lastNameHasError;

  String? _phoneMessageError;

  String? get phoneMessageError => _phoneMessageError;

  bool _phoneHasError = false;

  bool get phoneHasError => _phoneHasError;

  String? _birthdayMessageError;

  String? get birthdayMessageError => _birthdayMessageError;

  bool _birthdayHasError = false;

  bool get birthdayError => _birthdayHasError;

  String? _countryMessageError;

  String? get countryMessageError => _countryMessageError;

  bool _countryHasError = false;

  bool get countryHasError => _countryHasError;

  bool _isLoading = false;

  bool get isLoading => _isLoading;

  bool isSocial = false;


  final connectionChecker = ConnectionChecker();
  final _services = SignServices();
  final statusService = StatusService();
  final userService = UserServices();

  isLoadingChange(bool loading) {
    _isLoading = loading;
    update();
  }

  updateEmailError(String? text, bool isError) {
    _emailHasError = isError;
    _emailMessageError = text;
    update();
  }

  updateNameError(String? text, bool isError) {
    _nameHasError = isError;
    _nameMessageError = text;
    update();
  }

  updateLastNameError(String? text, bool isError) {
    _lastNameHasError = isError;
    _lastNameMessageError = text;
    update();
  }

  updateBirthdayError(String? text, bool isError) {
    _birthdayHasError = isError;
    _birthdayMessageError = text;
    update();
  }

  updateCountryError(String? text, bool isError) {
    _countryHasError = isError;
    _countryMessageError = text;
    update();
  }

  updatePhoneError(String? text, bool isError) {
    _phoneHasError = isError;
    _phoneMessageError = text;
    update();
  }

  updatePasswordError(String? text, bool isError) {
    _passwordHasError = isError;
    _passwordMessageError = text;
    update();
  }

  updateRepeatPasswordError(String? text, bool isError) {
    _repeatPasswordHasError = isError;
    _repeatPasswordMessageError = text;
    update();
  }

  setIsSocial(bool data){
    isSocial = data;
    update();
  }

  late Map<String, dynamic> dataBody;

  signData(name, lastName, birthday, country, phone, phone_code, email,
      password, repeatPassword) async {
    if(isSocial){
      dataBody = {
        "name": name,
        "lastName": lastName,
        "birthday": birthday,
        "country": country,
        "phone": phone,
        "phone_code": phone_code,
        "email": email,
      };
    }else{
      dataBody = {
        "name": name,
        "lastName": lastName,
        "birthday": birthday,
        "country": country,
        "phone": phone,
        "phone_code": phone_code,
        "email": email,
        "password": password,
        "password_confirmation": repeatPassword,

      };
    }
    List validation = validate(dataBody);
    updateNameError(null, false);
    updateLastNameError(null, false);
    updateBirthdayError(null, false);
    updateCountryError(null, false);
    if(!isSocial){
      updatePasswordError(null, false);
      updateRepeatPasswordError(null, false);
    }

    if (validation.isEmpty) {
      isLoadingChange(true);
      if (await connectionChecker.isOnline() == false) {
      } else {
        print(dataBody);
        isLoadingChange(false);
        return true;
      }
    } else {
      isLoadingChange(false);
      validation.forEach((element) {
        if (element["field"] == "email") {
          updateEmailError(element["message"], true);
        } else if (element["field"] == "name") {
          updateNameError(element["message"], true);
        } else if (element["field"] == "lastname") {
          updateLastNameError(element["message"], true);
        } else if (element["field"] == "birthday") {
          updateBirthdayError(element["message"], true);
        } else if (element["field"] == "country") {
          updateCountryError(element["message"], true);
        } else if (element["field"] == "phone") {
          updatePhoneError(element["message"], true);
        } else if(!isSocial){
          if (element["field"] == "password") {
            updatePasswordError(element["message"], true);
          } else if (element["field"] == "repeatpassword") {
            updateRepeatPasswordError(element["message"], true);
          }
        }
      });
    }
  }

  savePhoto(File? photo, locale) async {
    if (await connectionChecker.isOnline() == false) {
    } else {
      var response = await _services.register(dataBody,photo, locale);
      if(response["status"] == "success"){
        return true;
      }else{
        // Get.snackbar(response["message"], response["data"]["errors"]["email"][0]
        //     ,snackPosition: SnackPosition.BOTTOM);
        Get.snackbar(response["message"], response["data"]["errors"]["avatar"][0]
            ,snackPosition: SnackPosition.BOTTOM);
        return false;
      }
    }
  }

  completeProfile() async {
    final data = {
      "name": dataBody["name"],
      "last_name": dataBody["lastName"],
      "birthdate": dataBody["birthday"],
      "id_country": dataBody["country"],
      "phone" : dataBody["phone"],
      "email" : dataBody["email"],
      "phone_code" : dataBody["phone_code"],
    };
    final response = await _services.completeProfile(data);
    print("completeProfile" + response.toString());
    final user  = userFromJson(response["data"]["data"]["data"]);
    userService.setUser(userToJson(user));
    print("user" + user.toString());
    return response["isSuccess"];
  }

  verifyPhone(phone) async {
      var response = await _services.verifyPhone(phone);
      print(response);
      if(response["isSuccess"] == false){
        updatePhoneError('the_phone_already_exists'.tr, true);
      }else{
        updatePhoneError(null, true);
      }
      return response["isSuccess"];
  }
  checkEmail(String email) async{
    if(email.isNotEmpty){
      print(email);
      final response = await _services.checkEmail(email);
      print(response);
      if(response["data"]["status"] == "failure"){
        updateEmailError('The_email_already_exists'.tr, true);
      }else{
        updateEmailError(null, true);
      }
      return response["isSuccess"];
    }

  }


  List validate(data) {
    List errors = [];
    if (data["name"].toString().isEmpty) {
      errors.add({"field": "name", "message": 'The_name_is_required'.tr});
    }
    if (data["lastName"].toString().isEmpty) {
      errors.add({"field": "lastname", "message": 'Last_name_is_required'.tr});
    }
    if (data["birthday"].toString().isEmpty ||
        data["birthday"].toString() == "mm/dd/yyyy") {
      errors.add({"field": "birthday", "message": 'Date_is_required'.tr});
    } else {
      try {
        DateFormat.yMd().parse(data["birthday"].toString());
      } catch (e) {
        errors.add({"field": "birthday", "message": 'The_format_is_not_valid'.tr});
      }
    }
    if (data["country"].toString().isEmpty) {
      errors.add({"field": "country", "message": 'The_country_is_required'.tr});
    }
    if (data["phone"].toString().isEmpty) {
      errors.add({"field": "phone", "message": 'Telephone_is_required'.tr});
    }else if(_phoneHasError && _phoneMessageError != null){
      errors.add({"field": "phone", "message": _phoneMessageError});
    }
    if (data["password"].toString().isEmpty) {
      errors
          .add({"field": "password", "message": 'Password_is_required'.tr});
    } else if (data["password"].toString().length < 8) {
      errors.add({
        "field": "password",
        "message": 'The_password_must_be_at_least_8_characters'.tr
      });
    } else {
      if (data["password_confirmation"].toString().isEmpty) {
        errors.add({
          "field": "repeatpassword",
          "message": 'Password_is_required'.tr
        });
      } else if (data["password"].toString() !=
          data["password_confirmation"].toString()) {
        errors.add({
          "field": "repeatpassword",
          "message": 'Does_not_match_passwords'.tr
        });
      }
    }
    if (data["email"].toString().isEmpty) {
      errors.add({"field": "email", "message": "El email es requerido"});
    } else {
      if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(data["email"].toString()) ==
          false) {
        errors.add({"field": "email", "message": 'The_email_is_invalid'.tr});
      }
      if(_emailHasError && _emailMessageError != null){
        errors.add({"field": "email", "message": _emailMessageError});
      }
    }
    return errors;
  }
}
