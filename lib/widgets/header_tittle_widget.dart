import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HeaderTittleWidget extends StatelessWidget{
  String tittle;
  HeaderTittleWidget({Key? key,
    required this.tittle,
  }): super(key : key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(10),
          child: Text(
            tittle,
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
              color: Colors.black
            ),
          ),
        ),
        Divider(color: Color(0xffD3D3D3),)
      ],
    );
  }
}