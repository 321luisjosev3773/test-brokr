
import 'dart:io';
import 'package:test_brokr/pages/login/models/DataLogin.dart';

import '../../../utils/http_service.dart';
import '../../../utils/status_service.dart';
import '../../../utils/userService.dart';
import '../models/Failure.dart';


class LoginServices {

  final httpService = HttpService();
  final statusService = StatusService();
  final firebaseShared = FirebaseShared();

  loginWithPassword(data)async{
    data = {
      "email": data["email"],
      "password": data["password"],
      "os": Platform.operatingSystem,
      "type": "guest",
      "fcm_token": await firebaseShared.getFirebaseToken(),
      "language" : data["local"]
    };
    final response = await httpService.httpPost("api/auth/login", data, isClientAuth: true);
    return {"isSuccess": response["status"] == statusService.SUCCESS ? true : false, "data": response["data"], "message": response["message"]};
  }

  loginWithSocial(token, local)async{
    final data = {
      "fcm_token": await firebaseShared.getFirebaseToken(),
      "os": Platform.operatingSystem,
      "social_auth": "google",
      "type": "host",
      "language" : local,
      "token" : token
    };
    print(data);
    final response = await httpService.httpPost("api/auth/login_with_social", data, isClientAuth: true);
    print("loginWithSocial" +response.toString());

    return {"isSuccess": response["status"] == statusService.SUCCESS ? true : false, "data": response,"status" : response["status"]};
  }

  completeProfile(DataLogin dataLogin)async{
    final data = {
      "name": dataLogin.customer.name,
      "last_name": dataLogin.customer.lastName,
      "birthdate": dataLogin.customer.birthdate,
      "id_country": dataLogin.customer.idCountry,
      "phone" : dataLogin.customer.phone,
      "email" : dataLogin.customer.email,
      "phone_code" : dataLogin.customer.phoneCode,
    };
    print(data);
    final response = await httpService.httpPost("api/profile", data);
    print("completeProfile" +response.toString());

    return {"isSuccess": response["status"] == statusService.SUCCESS ? true : false, "data": response,"status" : response["status"]};
  }
}