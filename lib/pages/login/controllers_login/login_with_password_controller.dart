import 'package:get/get.dart';
import 'package:test_brokr/pages/login/models/DataLogin.dart';
import '../../../utils/ConnectionCheckerService.dart';
import '../../../utils/UserService.dart';
import '../../../utils/oauth_token_service.dart';
import '../../../utils/status_service.dart';
import '../models/User.dart';
import '../services/login_services.dart';

class LoginWithPasswordController extends GetxController {
  String? _emailMessageError;

  String? get emailMessageError => _emailMessageError;

  bool _emailHasError = false;

  bool get emailHasError => _emailHasError;

  String? _passwordMessageError;

  String? get passwordMessageError => _passwordMessageError;

  bool _passwordHasError = false;

  bool get passwordHasError => _passwordHasError;

  bool _isLoading = false;

  bool get isLoading => _isLoading;

  String? _email;
  String? get email => _email;

  String? _password;
  String? get password => _password;

  final connectionChecker = ConnectionChecker();
  final _loginServices = LoginServices();
  final statusService = StatusService();
  final oauthTokenService = OauthTokenService();
  final userService = UserServices();
  DataLogin? dataLogin;


  isLoadingChange(bool loading) {
    _isLoading = loading;
    update();
  }

  updateEmailError(String? text, bool isError) {
    _emailHasError = isError;
    _emailMessageError = text;
    update();
  }

  updatePasswordError(String? text, bool isError) {
    _passwordHasError = isError;
    _passwordMessageError = text;
    update();
  }

  loginValidation(String? email, String? password, String local) async {
    List validation = validate({"email": email, "password": password});
    updateEmailError(null, false);
    updatePasswordError(null, false);
    if (validation.isEmpty) {
      if (await connectionChecker.isOnline() == false) {
      } else {
        return true;
      }
    } else {
      validation.forEach((element) {
        if (element["field"] == "email") {
          updateEmailError(element["message"], true);
        } else if (element["field"] == "password") {
          updatePasswordError(element["message"], true);
        }
      });
      return false;
    }
  }

  Future<Map<String, String>> login(String? email, String? password, String local) async {

    _email = email;
    _password = password;
    update();
    isLoadingChange(true);
    final response = await _loginServices.loginWithPassword(
        {"email": email, "password": password, "local": local});
    print("login" + response.toString());
    isLoadingChange(false);
    if (response["isSuccess"]) {
      dataLogin = dataLoginFromJson(response["data"]["data"]);
      oauthTokenService.setOauthToken(dataLogin?.token ??"");
      if(dataLogin?.customer.completedProfile != "Y"){
        return {"result": "no complete"};
      }else{
        final user  = userFromJson(response["data"]["data"]["customer"]);
        userService.setUser(userToJson(user));
        return {"result": "complete"};
      }
    } else {
      if ("Email not found" == response["data"]["data"]["errors"]["error"][0]) {
        return {"result": "register"};
      }else{
        Get.snackbar(response["message"], response["data"]["data"]["errors"]["error"][0],
            snackPosition: SnackPosition.BOTTOM);
        return {"result": "error"};
      }
      }
  }

   completeProfile() async {
    final response = await _loginServices.completeProfile(dataLogin!);
    print("completeProfile" + response.toString());
    final user  = userFromJson(response["data"]["data"]["data"]);
    userService.setUser(userToJson(user));
    print("user" + user.toString());
    return response["isSuccess"];
  }

  List validate(data) {
    List errors = [];
    if (data["email"].toString().isEmpty) {
      errors.add({"field": "email", "message": 'Email_is_required'.tr});
    } else {
      if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(data["email"].toString()) ==
          false) {
        errors.add({"field": "email", "message": 'The_email_is_invalid'.tr});
      }
    }
    if (data["password"].toString().isEmpty) {
        errors.add({"field": "password", "message": 'Password_is_required'.tr});
    }
    return errors;
  }
}
