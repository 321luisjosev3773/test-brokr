
import 'package:shared_preferences/shared_preferences.dart';

class FirebaseShared {

  Future getFirebaseToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString("firebaseToken") ?? "";
  }
  void setFirebaseToken(token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("firebaseToken",token);
  }
  void removeFirebaseToken() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove("firebaseToken");
  }
}