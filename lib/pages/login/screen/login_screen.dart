import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../utils/Firebase.dart';
import '../../../utils/userService.dart';
import '../../../widgets/header_tittle_widget.dart';
import '../../../widgets/main_button_widget.dart';
import '../../../widgets/outlined_button_widget.dart';
import '../../../widgets/text_field_password_widget.dart';
import '../../../widgets/text_field_widget.dart';
import '../../dashboard/screen/dashboard_screen.dart';
import '../controllers_login/login_google_controller.dart';
import '../controllers_login/login_with_password_controller.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  TextEditingController controller = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _loginController = Get.put(LoginWithPasswordController());
  final _loginGoogleController = Get.put(LoginGoogleController());
  final firebaseAction = FirebaseAction();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    firebaseAction.requestPermission();
    firebaseAction.getToken();
    super.initState();
  }

  signGoogle() async {
    Locale locale = Localizations.localeOf(context);
    await _loginGoogleController.signInWithGoogle();
    final result =
        await _loginGoogleController.loginSocial(locale.languageCode);
    if (result != null) {
      if (result) {
        Get.offAll(Dashboard());
      } else {
        // Get.toNamed('/register', arguments: {"isSocial": true});
        Get.offAll(Dashboard());
      }
    }
  }

  login() async {
    Locale locale = Localizations.localeOf(context);
    final validation = await _loginController.loginValidation(
        controller.text, passwordController.text, locale.languageCode);
    if (validation) {
      final response = await _loginController.login(
          controller.text, passwordController.text, locale.languageCode);
      switch (response["result"]) {
        case "complete":
          Get.offAll(Dashboard());
          break;
        case "no complete":
          final response = await _loginController.completeProfile();
          if(response){
            Get.offAll(Dashboard());
          }
          break;
        case "register":
          Get.toNamed('/register', arguments: {'isSocial': false});
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    HeaderTittleWidget(tittle: 'Log_in_or_sign_up'.tr),
                    SizedBox(
                      height: 10,
                    ),
                    GetBuilder<LoginWithPasswordController>(
                      init: LoginWithPasswordController(),
                      builder: (_) => TextFieldWidget(
                          label: 'email'.tr,
                          type: TextInputType.emailAddress,
                          controller: controller,
                          hasError: _.emailHasError,
                          errorMessage: _.emailMessageError),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    GetBuilder<LoginWithPasswordController>(
                      init: LoginWithPasswordController(),
                      builder: (_) => TextFieldPasswordWidget(
                        hasError: _.passwordHasError,
                        errorMessage: _.passwordMessageError,
                        controller: passwordController,
                        label: 'password'.tr,
                        type: TextInputType.visiblePassword,
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Text(
                      'forgot_password'.tr,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Color(0xff75788D)),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    GetBuilder<LoginWithPasswordController>(
                      init: LoginWithPasswordController(),
                      builder: (_) => MainButtonWidget(
                          isLoading: _.isLoading,
                          label: 'continue'.tr,
                          backgroundColor: Color(0xff675DFE),
                          foregroundColor: Colors.white,
                          executeAction: login),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Flexible(child: Divider()),
                        SizedBox(
                          width: 5,
                        ),
                        Text('Or'.tr),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(child: Divider())
                      ],
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    GetBuilder<LoginWithPasswordController>(
                      init: LoginWithPasswordController(),
                      builder: (_) => OutlinedButtonWidget(
                        label: 'Continue_with_Google'.tr,
                        strokerColor: Colors.black87,
                        foregroundColor: Colors.black87,
                        backgroundColor: Colors.white,
                        executeAction: signGoogle,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox()
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
