// To parse this JSON data, do
//
//     final dataLogin = dataLoginFromJson(jsonString);

import 'dart:convert';

DataLogin dataLoginFromJson(Map<String, dynamic> str) => DataLogin.fromJson(str);

String dataLoginToJson(DataLogin data) => json.encode(data.toJson());

class DataLogin {
  Customer customer;
  String token;

  DataLogin({
    required this.customer,
    required this.token,
  });

  factory DataLogin.fromJson(Map<String, dynamic> json) => DataLogin(
    customer: Customer.fromJson(json["customer"]),
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "customer": customer.toJson(),
    "token": token,
  };
}

class Customer {
  int? id;
  String? stripeId;
  String? name;
  String? lastName;
  String? email;
  String? phoneCode;
  String? phone;
  String? birthdate;
  String? idCountry;
  String? street;
  String? city;
  String? state;
  String? zipCode;
  String? avatar;
  String? os;
  String? socialAuth;
  String? language;
  String? fcmToken;
  String? type;
  String? completedProfile;
  String? verifiedAsBroker;
  String? verifiedAsHost;
  String? verifiedAsAffiliate;
  String? additionalData;
  String? verifiedIdentity;
  String? rejectionReason;
  String? status;
  String? createdAt;
  String? updatedAt;

  Customer({
    required this.id,
    required this.stripeId,
    required this.name,
    required this.lastName,
    required this.email,
    required this.phoneCode,
    required this.phone,
    required this.birthdate,
    required this.idCountry,
     this.street,
     this.city,
     this.state,
     this.zipCode,
    required this.avatar,
    required this.os,
    required this.socialAuth,
    required this.language,
    required this.fcmToken,
    required this.type,
    required this.completedProfile,
    required this.verifiedAsBroker,
    required this.verifiedAsHost,
    required this.verifiedAsAffiliate,
     this.additionalData,
    required this.verifiedIdentity,
    this.rejectionReason,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    id: json["id"],
    stripeId: json["stripe_id"],
    name: json["name"],
    lastName: json["last_name"],
    email: json["email"],
    phoneCode: json["phone_code"],
    phone: json["phone"],
    birthdate: json["birthdate"],
    idCountry: json["id_country"],
    street: json["street"],
    city: json["city"],
    state: json["state"],
    zipCode: json["zip_code"],
    avatar: json["avatar"],
    os: json["os"],
    socialAuth: json["social_auth"],
    language: json["language"],
    fcmToken: json["fcm_token"],
    type: json["type"],
    completedProfile: json["completed_profile"],
    verifiedAsBroker: json["verified_as_broker"],
    verifiedAsHost: json["verified_as_host"],
    verifiedAsAffiliate: json["verified_as_affiliate"],
    additionalData: json["additional_data"],
    verifiedIdentity: json["verified_identity"],
    rejectionReason: json["rejection_reason"],
    status: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "stripe_id": stripeId,
    "name": name,
    "last_name": lastName,
    "email": email,
    "phone_code": phoneCode,
    "phone": phone,
    "birthdate": birthdate,
    "id_country": idCountry,
    "street": street,
    "city": city,
    "state": state,
    "zip_code": zipCode,
    "avatar": avatar,
    "os": os,
    "social_auth": socialAuth,
    "language": language,
    "fcm_token": fcmToken,
    "type": type,
    "completed_profile": completedProfile,
    "verified_as_broker": verifiedAsBroker,
    "verified_as_host": verifiedAsHost,
    "verified_as_affiliate": verifiedAsAffiliate,
    "additional_data": additionalData,
    "verified_identity": verifiedIdentity,
    "rejection_reason": rejectionReason ?? "",
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
