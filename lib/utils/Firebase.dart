import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:test_brokr/utils/userService.dart';

class FirebaseAction {
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  final firebaseShared = FirebaseShared();


  // request permission to get message from app
  void requestPermission() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }

  void getToken() async {
    var token = await firebaseShared.getFirebaseToken();
    if(token == null || token == ""){
      token = await FirebaseMessaging.instance.getToken();
      firebaseShared.setFirebaseToken(token);
    }
    print("this is the token" + token.toString());

  }
}